package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"time"
)

type Servers struct {
	exists map[string]bool
	childs []Server
	chn    chan Server
}

type Server struct {
	Url   string `json:"url"`
	Delay int    `json:"delay"`
}

func (sr *Server) Beat() (*http.Response, error) {
	return http.Get(sr.Url)
}

func StartHeartBeater(servers *Servers) {
	go func() {
		for _, server := range servers.childs {
			servers.chn <- server
		}
	}()
	for server := range servers.chn {
		fmt.Println("Recieveing", server)
		servers.childs = append(servers.childs, server)
		go func(server Server) {
			ticker := time.NewTicker(time.Second * time.Duration(server.Delay))
			for {
				select {
				case <-ticker.C:
					fmt.Println("ticking", server)
					_, err := server.Beat()
					if err != nil {
						fmt.Println("Error", err)
						continue
					}
				}
			}
		}(server)
	}
}

var mainServers = Servers{
	childs: []Server{},
	chn:    make(chan Server),
	exists: make(map[string]bool),
}

func HttpLogger(fn func(http.ResponseWriter, *http.Request)) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		starTime := time.Now()
		fn(w, r)
		fmt.Printf("[%v] [%s] %s %v\n", time.Now().UTC(), r.Method, r.URL.Path, time.Since(starTime))
	})
}

func main() {
	go StartHeartBeater(&mainServers)
	router := http.NewServeMux()
	router.Handle("GET /health", HttpLogger(HealthCheckHandler))
	router.Handle("GET /servers", HttpLogger(ListServers))
	router.Handle("POST /add", HttpLogger(AddServer))
	port := os.Getenv("PORT")
	if port == "" {
		port = "8080"
	}
	port = ":" + port
	fmt.Println("Listening on ", port)
	http.ListenAndServe(port, router)
}

func HealthCheckHandler(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("Hello, World !"))
}

func AddServer(w http.ResponseWriter, r *http.Request) {
	var server Server
	err := json.NewDecoder(r.Body).Decode(&server)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if ok := mainServers.exists[server.Url]; !ok {
		mainServers.chn <- server
		mainServers.exists[server.Url] = true
	}
	w.WriteHeader(http.StatusCreated)
}

func ListServers(w http.ResponseWriter, r *http.Request) {
	data, err := json.Marshal(mainServers.childs)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(data)
}
